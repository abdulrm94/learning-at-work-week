# Learning at Work Week

## Day 1 - Programming Basics
Start by forking the repo by:
- Go to this [repo](https://gitlab.com/WesleyHindle/learning-at-work-week)
- Click `fork` in the top right


### Task

Open `calculator.py` and use the addition function as a basis for the other functions. Help can be found [here](https://programmingwithmosh.com/wp-content/uploads/2019/02/Python-Cheat-Sheet.pdf)

Go to [this site](https://www.programiz.com/python-programming/online-compiler/), where we will write our code.

### Before tomorrow:
- Please create an account on https://labs.play-with-docker.com/



---
## Day 2 - Containers

Containers are a lightweight way of ensuring applications which are portable, consistent and used within micro service architectures. 

Advantages: 
- Scalability 
- Portability
- Consistency

Downsides:
- Not being suitable to all architectures (e.g. monoliths)
- Not suitable for apps which have long-running process.
- Not suitable for all app types (e.g. those which require access to certain hardware)

---

The main way people use containers is with Docker. To keep it simple we'll be using a website rather than a local instance of Docker. Go to https://labs.play-with-docker.com/ and we'll aim to build a Dockerfile so that you can containerise the app you build yesterday. Some basic docker commands can be found [here](https://www.edureka.co/blog/docker-commands/).

1. Once logged in, click on `Start`
2. On the right click `Start new instance` and you will see a terminal (black screen with coloured text).

--- 
#### Terminal basics
You will spend a lot of your time within the terminal, so learning how to do the basics is essential. Here we'll want to create two files, one for the app `calculator.py` and one for the dockerfile `Dockerfile`. 

A basic guide to getting started can be found [here](https://tutorialshut.com/unix-file-system-commands/). *There is more than one way to create a file, either is fine to use here*

We will be using vim (a text editor) to amend your files. To save and close files in vim press `esc` and then type `:wq` which will write the changes you have made and then close the file.


---
### Task
Take the `calculator.py` app we created yesterday and containerise it. 

- Create a file for your calculator and another for your Dockerfile (do not use directories)
- Copy the code from the app you created yesterday and paste it into the `calculator.py` file you have created using vim. 
- Write a Dockerfile for your calculator app and get it to run with Docker
Your Dockerfile should contain:
    - A base image capable of running python by default - [look here](https://hub.docker.com/search?q=)
    - Set a directory where you want the app to run from
    - Tell the Dockerfile where you want the app to be copied from (on your machine) and where you want it to go (in the image)
    - Command telling the image how to run the application 
- Once working on the labs with docker site, copy your `Dockerfile`'s content and paste it into a file in this repo. The file should be called `Dockerfile`.



---
## Day 3 - Pipelines
Pipelines are the basis of [continuous integration and continuous development](https://www.redhat.com/en/topics/devops/what-is-ci-cd) (CI/CD). They allow us to automate repeated processes for consistent results reducing the amount of manual work required. They run the jobs assigned to them reliably and consistently. 

Automation is a huge part of DevOps and is one of the biggest tools we use to break down the silos between software development (the dev!) and software operations (the ops!).

### Task

Take the `calculator.py` file and the `Dockerfile` you created over the past two days and containerise and run the application in the pipeline. 

- Ensure you have your `calculator.py` and `Dockerfile` from earlier in the week.
- Create a `.gitlab-ci.yml` file
- Add a build and run stage ([hint](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html))
- Select an appropriate image to be used in the stages ([hint](https://hub.docker.com/_/docker))
- Ensure the built image can be used in the next step ([hint](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html))




### Stretch task
If you've finished the above add in two stages which lint and test your python code. Available packages to install found [here](https://pypi.org/) and are installed using `pip install <package-name>`.


- Ensure these stages are added in an appropriate order in the pipeline.
- Add a linting stage which is allowed to fail ([hint](https://docs.gitlab.com/ee/ci/yaml/#allow_failure)) and uses the `flake8` linter.
- Add a test stage which will run unit tests on your `calculator.py` app. Use the `pytest` library ([hint](https://docs.pytest.org/en/7.3.x/)).



---
## Day 4 - Terraform

Terraform is an infrastructure-as-code (IAC) tool used for creating infrastructure in a codified way. This reduces the need to manually create and manage infrastructure, which is notoriously inconsistent. The basic flow for Terraform is `validate > plan > apply > destroy`

### AWS 

AWS is a cloud service provider used for running infrastructure including storage and virutal machines.

#### S3
[S3](https://aws.amazon.com/s3/) is a cloud storage service used for long term storage of objects such as files, or images.


#### Elastic Container Registry (ECR)
[ECR](https://aws.amazon.com/ecr/) is a container registry where you can store containers which can easily be deployed.

#### IAM Roles
[IAM](https://aws.amazon.com/iam/) offers security controls over access to AWS services. One way of doing this is via roles which is a way of applying a set of policies (e.g. what services they can access and how) to a user within the AWS account.


The Terraform AWS documentation can be found [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs) to help you with the task below.

--- 

Before starting the task you will need to create your AWS account. Details for deleting your account are at the bottom of the page. We **highly** recommend closing your account as we have gone through the setup in the simplest, not most secure, manner.

#### Creating your AWS account
- Go [here](https://portal.aws.amazon.com/billing/signup#/start/email) to create your AWS account
- Use your **real details** when creating your account (if you need assistance from AWS you'll need to know your details)
- You will need to add a card to your account. This will **not** charge you for having an account. 
- For the first year you have access to a lot of free services, but not all services, or offerings within a service are free and you will be charged if you excees the free limits. 
- If you're unlikely to use your AWS account again after this week, then feel free to close it, so any free tier items you use don't get charged after 12 months.
- For support plan leave it on the free tier
- If you ever want to check if you're being charged for any services you're using, go to [Billing Management Console](https://us-east-1.console.aws.amazon.com/billing/home?region=eu-west-2#/)



### Task


You need to have an AWS account before we are able to check whether AWS resources are able to be completed.

- Create an access key and in your AWS account (don't move away from the page once created!)
- Add these details as environment variables to your repo settings. The key must be `TF_VAR_<var-name>
- Create a file for your terraform provider with the information about the AWS region (`eu-west-2`) and version of the provider to use (`3.75.2`).
- Add the details of your access key and access key ID to `provider.tf`, so you can communicate with AWS.
- Create a `resources.tf` file which creates the following resources:
  - An S3 bucket with a tag of your name.
  - An Elastic Container Registry with a tag of 'my_first_ecr'
  - **Stretch** - An IAM role with ecr as the service.
- Add a `terraform validate` stage to your .gitlab-ci.yml to check your .tf files are correct.
- Add your AWS first resource and then check you're able to run a `terraform plan` in a second pipeline stage
<!-- Check this last step with Wes -->









--- 
## Day 5 - Applying Terraform & Finishing Pipeline

Finish pipeline 

Artifacts are files and/or directories which are passed from one stage to another. They help reduce repeated processes, saving time and money.

### Task
Put the pipelines you have written this week together and deploy your Terraform infrastructure.

- Add a `terraform apply` stage
- Add a `terraform destroy` stage which has to be manually triggered ([hint](https://docs.gitlab.com/ee/ci/jobs/job_control.html#rules-examples)).
<!-- -auto-destroy rather than manual? -->
- Once applied, check your resources exist in the [AWS console](https://signin.aws.amazon.com/signin?redirect_uri=https%3A%2F%2Fconsole.aws.amazon.com%2Fconsole%2Fhome%3FhashArgs%3D%2523%26isauthcode%3Dtrue%26state%3DhashArgsFromTB_eu-north-1_91e093c17dce8e62&client_id=arn%3Aaws%3Asignin%3A%3A%3Aconsole%2Fcanvas&forceMobileApp=0&code_challenge=eyLGjVBe6PlS1d9PO9LtQ_LUhkvmF3lSA2z7g0QNdSM&code_challenge_method=SHA-256) (look under S3, ECR & IAM services)

Stretch: 
- Add the pipeline from earlier in the week which tested, linted, built and ran your calculator application.


**Before finishing today make sure your destroy stage has run successfully**

---

## Deleting your AWS account

- Manually check your AWS resources have been deleted in the AWS console. 
- Once you're sure they have been deleted go [here](https://us-east-1.console.aws.amazon.com/billing/home#/account)
- Scroll to the bottom of the page and select close account.

---

## Next Steps
If you enjoyed this week and want to work on a DevOps project one you can try is: 
- Write a Python application (something more complicated than a calculator!) e.g. [weather data app](https://www.youtube.com/watch?v=SqvVm3QiQVk&t=1494s)
- Use Gitlab as your distributed version control system.
- Deploy your application as an AWS Lambda. As part of a lambda you will need to have:
  * iam role
  * log group for the metrics lambdas produce
  * package your application as a .zip 
- Deploy your lambda using Terraform